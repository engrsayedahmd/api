<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 3/30/19
 * Time: 3:00 PM
 */

namespace token;

require_once 'Core.php';

use \core\Core;

header('Content-Type: application/json');

class Token extends Core {

    public static function getToken() {

        return json_encode(array(
            "access_token" => "ea14461f-96bc-3638-9145-f3bb06967ee8",
            "refresh_token" => "f3a4a407-ae47-31d1-b586-a3d866d6ff83",
            "scope" => "default",
            "token_type" => "Bearer",
            "expires_in" => 2412
        ), JSON_PRETTY_PRINT);
    }

}