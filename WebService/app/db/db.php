<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 3/30/19
 * Time: 10:51 AM
 */

define("MYSQL_HOST", "localhost");
define("MYSQL_DB", "airtel");
define("MYSQL_CHAR", "utf8");
define("MYSQL_USER", "root");
define("MYSQL_PASS", "12345");
define("SECRET_KEY", "CodeB0xx");

try {
    $db = new PDO(
        "mysql:host=".MYSQL_HOST.";dbname=".MYSQL_DB.";charset=".MYSQL_CHAR,
        MYSQL_USER, MYSQL_PASS, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ]
    );
} catch (Exception $ex) { die($ex->getMessage()); }

