<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 3/30/19
 * Time: 3:02 PM
 */

require_once 'core/Token.php';
require_once 'db/db.php';

use token\Token;

$auth_token = null;

Token::initDB($db);

/**
 * get content type
 */
function getContentType() {

    if (isset($_SERVER['CONTENT_TYPE'])) {
        return $_SERVER['CONTENT_TYPE'];
    } else {
        return null;
    }
}

/**
 * get authorization header
 */

function getAuthorizationHeader(){

    $headers = null;
    if (isset($_SERVER['Authorization'])) {
        $headers = trim($_SERVER["Authorization"]);
    }
    else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
    } elseif (function_exists('apache_request_headers')) {
        $requestHeaders = apache_request_headers();
        // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
        $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        //print_r($requestHeaders);
        if (isset($requestHeaders['Authorization'])) {
            $headers = trim($requestHeaders['Authorization']);
        }
    }
    return $headers;
}

/**
 * get access token from header
 * */
function getBearerToken() {

    $headers = getAuthorizationHeader();
    // HEADER: Get the access token from the header
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    return null;
}

function getBasicToken() {

    $headers = getAuthorizationHeader();
    // HEADER: Get the access token from the header
    if (!empty($headers)) {
        if (preg_match('/Basic\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    return null;
}

function isAuthenticated() {

    global $auth_token;

    $username = 'MCTF@Silver';
    $password = 'sdfd5465s4dfsdf6545';

    if ($_POST['username'] == $username && $_POST['password'] == $password) {
        $auth_token = 'sfd5sdf4sdf4sdf4';
        return true;
    } else {
        return false;
    }
}

function generateToken() {

    global $auth_token;

    if (isAuthenticated()) {

        if (!is_null($auth_token)) {

            return Token::getToken();

        } else {
            return json_encode(array(
                "error_description" => "Client Authentication failed.",
                "error" => "invalid_client"
            ));
        }


    } else {

        return json_encode(array(
            "error_description" => "Authentication failed for $username",
            "error" => "invalid_grant"
        ), JSON_PRETTY_PRINT);
    }

}